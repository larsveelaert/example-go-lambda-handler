module bitbucket.org/persgroep/columbus

go 1.12

require (
	github.com/apex/gateway v1.1.1
	github.com/aws/aws-lambda-go v1.9.0
	github.com/aws/aws-sdk-go v1.18.2
	github.com/djhworld/go-lambda-invoke v0.0.3
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	github.com/tj/assert v0.0.0-20171129193455-018094318fb0
)
