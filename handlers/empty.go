package handlers

import (
	"fmt"
	"net/http"
)

//Empty is a placeholder for a healthcheck
func (h *Handlers) Empty(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "")
}
