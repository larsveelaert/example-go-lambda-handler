package main

import (
	"bitbucket.org/persgroep/columbus/gateway"
	"bitbucket.org/persgroep/columbus/handlers"
	"log"
	"net/http"
	"os"
	"strings"
)

var env = os.Environ()
var addr = "127.0.0.1:8080"
var h = &handlers.Handlers{}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", h.Empty)

	if inLambda(env) {
		log.Fatal(gateway.ListenAndServe("", mux))
	} else {
		log.Fatal(http.ListenAndServe(addr, mux))
	}
}

func inLambda(env []string) bool {
	var lambdaTaskRootEnv bool
	var awsExecutionEnv bool
	for _, e := range env {
		if strings.HasPrefix(e, "LAMBDA_TASK_ROOT=") {
			lambdaTaskRootEnv = true
		}
		if strings.HasPrefix(e, "AWS_EXECUTION_ENV=") {
			awsExecutionEnv = true
		}
	}
	if lambdaTaskRootEnv && awsExecutionEnv {
		return true
	}
	return false
}
