package main

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/rpc"
	"os"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestInLambda(t *testing.T) {
	var tests = []struct {
		env    []string
		result bool
	}{
		{[]string{"LAMBDA_TASK_ROOT=", "AWS_EXECUTION_ENV="}, true},
		{[]string{"LAMBDA_TASK_ROOT="}, false},
		{[]string{"AWS_EXECUTION_ENV="}, false},
		{[]string{""}, false},
		{[]string{"TEST_ENV="}, false},
	}
	for _, test := range tests {
		t.Run(strings.Join(test.env, ","), func(t *testing.T) {
			assert.Equal(t, test.result, inLambda(test.env))
		})
	}
}

func TestMainStart(t *testing.T) {
	var wg sync.WaitGroup

	//testing http
	wg.Add(1)

	addr = "127.0.0.1:8080"

	go main()
	time.Sleep(2 * time.Second)

	resp, err := http.Get("http://" + addr + "/")
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 200, resp.StatusCode)
	wg.Done()

	//testing lambda
	wg.Add(1)

	envOrg := env
	env = []string{"LAMBDA_TASK_ROOT=1", "AWS_EXECUTION_ENV=1"}
	defer func() { env = envOrg }()

	os.Setenv("_LAMBDA_SERVER_PORT", "9988")
	go main()
	time.Sleep(2 * time.Second)

	_, err = rpc.Dial("tcp", "127.0.0.1:9988")
	if err != nil {
		t.Fail()
	}
	wg.Done()
}

func TestMainHandlers(t *testing.T) {
	//check if right handler is called
	var tests = []struct {
		path       string
		statusCode int
	}{
		{"/", 200},
	}

	addr = "127.0.0.1:8080"
	go main()
	for _, test := range tests {
		t.Run(test.path, func(t *testing.T) {
			resp, err := http.Get("http://" + addr + test.path)
			if err != nil {
				t.Error(err)
			}
			assert.Equal(t, test.statusCode, resp.StatusCode)
		})
	}
}
